#!/usr/bin/env python3

import argparse
import json
import pathlib
import os

from collections.abc import Mapping
from urllib import parse as urlparse

import requests
import jsondiff


MY_PATH = pathlib.Path(os.path.realpath(__file__)).parent


## TODO: this fn should be common code
def normalize_api_version(api_ver):
    return api_ver.replace('.', '_').replace('-', '_')


def parse_args():
    pa = argparse.ArgumentParser()

    pa.add_argument('-u', '--url',
        action='append', required=True,
        help='url(s) pointing to open api specification(s)'
    )

    pa.add_argument('-t', '--type', 
        choices=['sonarnexus3'],
        help='optionally one can specify the kind of server which are'\
             ' queried for open api spec, this allows some internal'\
             ' optimization like properly defaulting the standard REST path'
    )

    pa.add_argument('--default-urlpath', 
        help='the urlpath to use for "incomplete" query urls'
    )

    return pa.parse_args()


def run(args):
    default_urlpath = args.default_urlpath
    urltype = args.type

    if urltype == 'sonarnexus3':
        default_urlpath = "/service/rest/swagger.json"

    for u in args.url:
        # append url path (optional)
        un = urlparse.urlparse(u)

        if not un.path:
            assert default_urlpath, \
               "Failed to normalize query url '{}', either add path"\
               " to it or set a default_path".format(u)

            un = un._replace(path=default_urlpath)

        un = urlparse.urlunparse(un)

        print("Query open api url: {}".format(un))

        # GET url, expect it to return an OAS
        oas = requests.get(un)
        oas.raise_for_status()
        oas = oas.json()

        # check if result json is really an OAS
        assert isinstance(oas, Mapping), \
           "Valid OAS must be a dict / mapping, but was of"\
           " type '{}':\n{}".format(type(oas), oas)

        valid_selfver_keys = [('openapi', 3), ('swagger', 2)]

        spec_ver = None
        matched_key = None

        for k in valid_selfver_keys:
            spec_ver = oas.get(k[0], None)

            if spec_ver:
                matched_key = k
                break

        msg = "Returned json from querying url '{}' seems not"\
              " to be a valid OAS".format(un)

        assert spec_ver, msg + \
          ", as it does not contain one of the mandatory"\
          " self version keys: {}\n{}".format(valid_selfver_keys, oas)

        assert isinstance(spec_ver, str), msg + \
          ", self version value must be of type string, but"\
          " was of type '{}': {}".format(type(spec_ver), spec_ver)

        try:
            vmajor = int(spec_ver.split('.')[0])
        except ValueError as e:
            raise Exception(msg + \
               ". Could not parse version major as int: '{}'".format(spec_ver)
            ) from e 

        assert vmajor >= matched_key[1], msg + \
          ", as found version major number '{}' was smaller than the"\
          "  expected minimum value '{}' for matched self version"\
          " key '{}'".format(vmajor, matched_key[1], matched_key[0])

        # get api version from info field
        apiver = oas['info']['version']

        # create correct api spec filename from api version
        apispec_path = MY_PATH / '..' / 'resources' \
           / 'api_specs' / "{}.json".format(normalize_api_version(apiver))

        # check if spec file for this version already exists ...
        apispec_path = apispec_path.resolve()
        if apispec_path.exists():
            assert apispec_path.is_file(), \
               "Local api spec path '{}' does already exist,"\
               " but is not a regular file".format(apispec_path)

            # ... if so, compare the content to queried OAS, create 
            # error when different
            with apispec_path.open('r') as f:
                old = json.load(f)

            diff = jsondiff.diff(old, oas, syntax='symmetric', marshal=True)
            assert not diff, \
              "OAS returned by querying url '{}' differs from already"\
              " existing local OAS inside spec file '{}' for given"\
              " api version '{}':\n{}".format(un, 
                  apispec_path, apiver, json.dumps(diff, indent=2)
              )

            continue

        # ... if not, create it
        with apispec_path.open('w') as f:
            json.dump(oas, f, indent=2)



if __name__ == '__main__':
    run(parse_args())

