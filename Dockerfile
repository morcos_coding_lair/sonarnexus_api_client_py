
FROM python:3-alpine

ARG http_proxy
ARG https_proxy
ARG no_proxy

ARG HTTP_PROXY
ARG HTTPS_PROXY
ARG NO_PROXY

ARG B_CMD

WORKDIR /usr/src/app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD "$B_CMD"

