#!/usr/bin/env sh

_mydir="$(readlink -f "$0" | xargs dirname )"

##
## note: because of the type we set here first url and 2nd 
##   url are internally normalized to the same final query url
##
"${_mydir}/../../tools/get_api_specs.py" \
  --type 'sonarnexus3' \
  --url "https://repositories.developer.sunnyportal.com/service/rest/swagger.json" \
  -u "https://repositories.developer.sunnyportal.com"

