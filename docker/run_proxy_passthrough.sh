#!/usr/bin/env bash

_my_dir="$(readlink -f "$0" | xargs dirname )"


http_proxy="${http_proxy:-${HTTP_PROXY}}"
https_proxy="${https_proxy:-${HTTPS_PROXY:-${http_proxy}}}"
no_proxy="${no_proxy:-${NO_PROXY}}"


dargs=(
  -e "http_proxy=${http_proxy}"
  -e "HTTP_PROXY=${http_proxy}"

  -e "https_proxy=${https_proxy}"
  -e "HTTPS_PROXY=${https_proxy}"

  -e "no_proxy=${no_proxy}"
  -e "NO_PROXY=${no_proxy}"

  --net=host
)

export DOCKER_BUILD_SCRIPT="build_proxy_passthrough.sh"
export DOCKER_RUN_ARGS="${dargs[*]}"
"${_my_dir}/run.sh" "$@"

