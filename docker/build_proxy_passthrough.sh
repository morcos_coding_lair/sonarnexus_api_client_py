#!/usr/bin/env bash

_my_dir="$(readlink -f "$0" | xargs dirname )"

fn_get_proxy() {
  echo "$1" | sed 's/\(localhost\)\|\(127.0.0.1\)/host.docker.internal/'
}


http_proxy="$(fn_get_proxy "${http_proxy:-${HTTP_PROXY}}")"
https_proxy="$(fn_get_proxy "${https_proxy:-${HTTPS_PROXY:-${http_proxy}}}")"
no_proxy="${no_proxy:-${NO_PROXY}}"


bargs=(
  --build-arg "http_proxy=${http_proxy}"
  --build-arg "HTTP_PROXY=${http_proxy}"

  --build-arg "https_proxy=${https_proxy}"
  --build-arg "HTTPS_PROXY=${https_proxy}"

  --build-arg "no_proxy=${no_proxy}"
  --build-arg "NO_PROXY=${no_proxy}"

  --add-host=host.docker.internal:host-gateway
)


"${_my_dir}/build.sh" "${bargs[@]}" "$@"

