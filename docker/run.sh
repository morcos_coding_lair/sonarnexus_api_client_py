#!/usr/bin/env bash

_mydir="$(readlink -f "$0" | xargs dirname )"

user_id="$(id -u)"

builder="${DOCKER_BUILD_SCRIPT:-build.sh}"

if [ -n "${builder}" ]; then

  if [ "${builder:0:1}" != "/" ]; then
    builder="${_mydir}/${builder}"
  fi

  tmp="$("${builder}")" || exit "$?"
fi

img="${DOCKER_RUN_IMAGE:-${tmp}}"

repo_dir="$(dirname "${_mydir}")"

set -x
docker run --rm --userns=host \
  -ti \
  -u "${user_id}:${user_id}" \
  -w "${repo_dir}" \
  -v "${repo_dir}":"${repo_dir}" \
  ${DOCKER_RUN_ARGS} \
  "${img}" "$@"

