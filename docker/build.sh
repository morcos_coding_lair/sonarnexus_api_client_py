#!/usr/bin/env bash

_mydir="$(readlink -f "$0" | xargs dirname )"


imgname="mwilhelmi/sonarnexus_api_client_py"
imgtag="latest"


docker build "${_mydir}/.." -t "$imgname:$imgtag" $@ >&2

echo "$imgname:$imgtag"

